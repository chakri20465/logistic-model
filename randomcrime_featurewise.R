#----------------------------------------------------Uniqueness of Estimated series with random crime------------------------------------
vector2<-data.frame()
rand_crime1<-sample(1:1278, 1)
print(rand_crime1)
rcrime<-profiles[rand_crime1,11:ncol(profiles)]
for(d in 1:1278){
  colvalues1=0
  uq1=0
  for(gmi in 1:ncol(gm1))
  {
    if(median_all[d,gmi]==rcrime[gmi])
    {
      colvalues1=colvalues1+1
    }
  }
  uq1=92-colvalues1
  #cat(sprintf("%d - %d",uq1,colvalues1),"\n")
  vector2<-rbind(vector2,uq1)
  #cat(sprintf("%d ",colvalues1),"\n")
}
vector2<-vector2/100
quantile(vector2$X22, probs=seq(0,1,0.25),na.rm=F,names=T,type=7 ) 
summary(vector2$X22)
sd(vector2$X22)
plot(vector2$X22)
