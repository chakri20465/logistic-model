library(aod)
library(rms)
library(ggplot2)
library(caret)
library(ROCR)
library(Epi)
library(foreign)
library(ResourceSelection)
library(ModelGood)
complete<-read.csv("/home/chakri/Desktop/combined.csv")
names(complete)
complete<-complete[,-1]
names(complete)
set.seed(19568) # For reproducibile purpose
row_shf<-complete
conn<-row_shf$Conn
row_shf<-row_shf[,-(1:3)]
scale.row_shf<-scale(row_shf)
row_shf$Conn<-conn
complete<-complete[,-(1:2)]
data_partition <- createDataPartition(complete$Conn, p=0.70, list=F)
trainData <- complete[data_partition, ]
testData <- complete[-data_partition, ]
x = testData$Conn
ddist<- datadist(trainData)
options(datadist='ddist')
gen_model<-lrm(formula = Conn ~ Temporal + Spatial + Entry + Target + Goods + Victim + Trace, data = trainData,x=T,y=T,weight=mat1,method = "lrm.fit",scale = TRUE)
gen_model
summary(gen_model)
testData<-s
model_predict<-predict(gen_model,testData,type="fitted.ind") 
pred_matrix<-matrix(c(0),nrow = dim(testData)[1],ncol = 1)
tableres<-function(estimates,pred_matrix)
{
  for(i in 1:dim(testData)[1])
  {
    if(estimates[i]>0.06)
    {
      print(testData[i,])
      pred_matrix[i]=1
    }
  }
  return (pred_matrix)
}
finalestimation<-tableres(model_predict,pred_matrix)
cmatrix<-table(finalestimation,testData$Conn)
cmatrix
