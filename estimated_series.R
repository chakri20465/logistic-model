# check for z score once - might be giving good perdictions
library(aod)
library(rms)
library(ggplot2)
library(caret)
library(ROCR)
library(Epi)
library(foreign)
library(ResourceSelection)
library(ModelGood)
library(caret)
luniq<-read.csv("/home/chakri/Desktop/Martin/Files/Unique/linked_unique.csv")
uniq<-read.csv("/home/chakri/Desktop/Martin/Files/Unique/unlinked_unique.csv")
colnames(luniq)<- c("sno","id1","id2","Conn","Temporal","Spatial","Combined","Entry","Target","Goods","Trace","Victim")
colnames(uniq)<- c("sno","id1","id2","Conn","Temporal","Spatial","Combined","Entry","Target","Goods","Trace","Victim")
linked<-read.csv("/home/chakri/Desktop/Martin/Files/linked.csv")
unlinked<-read.csv("/home/chakri/Desktop/Martin/Files/unlinked.csv")
colnames(linked)<- c("sno","id1","id2","Conn","Temporal","Spatial","Combined","Entry","Target","Goods","Trace","Victim")
colnames(unlinked)<- c("sno","id1","id2","Conn","Temporal","Spatial","Combined","Entry","Target","Goods","Trace","Victim")
set.seed(19568) # For reproducibile purpose
trainlinked<-linked[c(11,   227 ,   26  , 210   ,  6 ,  217 ,  207  , 123 ,  142,   192 ,  187 ,  211  , 152 , 214 ,  116 ,   96 ,
                    193  ,  20 ,  113   , 34   , 73   ,  2 ,   83  , 121 ,  179  , 144   , 40 ,   77 ,   39   ,163,     3,    72 ,
                    143 ,   78  , 170 ,   27 ,   23  ,  38 , 228  , 112   ,  110  ,  55  , 185 ,  129  ,  48 ,   45 ,   69 ,
                    60 ,  173 ,  182  , 103 ,  147  ,  36  ,  13  , 130   , 99 ,  127 , 174  , 157 ,   22 ,  128 ,   57 ,  223 ,
                    92 ,  219 , 139 ,  137   , 46  , 184  , 196  , 154   , 93  , 101  ,  59  ,  74  , 171  , 221  , 107 ,  109 ),-(1:3)]
testlinked<-linked[c(101:228),-(1:3)]
trainunlinked<-unlinked[c(3833 , 9206, 11381, 14567,  2563 ,11509 ,13080, 17149 , 4522, 15199 , 12248, 11514 ,12802 , 7098 ,  836 ,13126 ,
                        14556 , 9133, 14936 , 2123 , 1721 ,11788, 17161, 13222 , 5512 , 3936 , 6452, 13787 ,14104 , 6660 , 9102 , 7817 ,
                        14589,  3730, 13218 , 4726 ,15350 , 8977 ,14213, 12606 , 5401 , 9398 , 9947 ,  927 , 8351, 12155,  1987, 15347 ,
                        12567 ,12574,  3086 ,14989 ,10246 , 3729 , 2466, 13988 , 6129 , 7547 , 1947, 14239 ,15810 , 5088, 12612,  7087 ,
                        393 , 6682,  4239 , 7989 , 5523 ,15026 , 2786, 17029, 11801 , 3895, 13103, 16949, 12789 ,12476, 14197,   445 ),-(1:3)]
testunlinked<-unlinked[c(101:228),-(1:3)]
s<-rbind(trainlinked,trainunlinked)
q<-rbind(testlinked,testunlinked)
#r<-read.csv("/home/chakri/Desktop/Martin/Files/Outputs/tr247.csv")
#q<-r[,-(1:2)]
#q<-uniq
names(s)
head(s)
tail(s)
dim(s)
print("----------------------------------------------------------------")
names(q)
head(q)
tail(q)
dim(q)
print("----------------------------------------------------------------")
ddist<-datadist(s)
options(datadist = 'ddist')
#level<-c(1.0725, 1.0254, 0.00048, 4.79910, 2828.618, 15.89921, 2884.257)
fimodel<-lrm(formula = Conn ~ Temporal + Spatial +  Entry + Target + Goods + Victim + Trace,data = s,x = T, y = T)
fimodel$coefficients
ratio<-fimodel$linear.predictors
cids<-fimodel$y
plot(cids,ratio,xlab="CID",ylab="Ratio",col = ifelse(ratio < 0,'red','green'), pch = 19 )
xyplot(cids ~ ratio, data=s, auto.key=list(space="right"), jitter.x=TRUE, jitter.y=TRUE)
#-----------------------------------------------------------------------------------#
#valid <- validate(fimodel, method="boot", B=70)
#valid
#-----------------------------------------------------------------------------------#
#result<-predict(fimodel,q)
result<-predict(fimodel, q, type="fitted.ind")     
plot(result,ylim=c(0,1))
result
minmaxresult<-result
zscoreresult<-result
#-------------------------minmax normalization--------------------------------------#
rmean<-mean(minmaxresult)
rmin<-min(minmaxresult)
rmax<-max(minmaxresult)
for(i in 1:length(minmaxresult))
{
  minmaxresult[i]=(minmaxresult[i]-rmin)/(rmax-rmin)
}
minmaxresult
#-----------------------------------------------------------------------------------#
#-------------------------zscore normalization--------------------------------------#
zscoreresult<-as.data.frame(scale(zscoreresult))
zscoreresult
#-----------------------------------------------------------------------------------#
predmatrix<-matrix(c(0),nrow = dim(q)[1],ncol = 1)
tableres<-function(estimated,predmatrix)
{
  for(i in 1:dim(q)[1])
  {
    if(estimated[i]>=0.67 ) # 0.67 - prediction ratio for for distinguishing linking and unlinking
    {
      #print(r[i,])
      predmatrix[i]=1
    }
  }
  return (predmatrix)
}
finalestimations<-tableres(result,predmatrix)
graph<-table(finalestimations,q$Conn)
graph
cresult

confusionMatrix(finalestimations,q$Conn)
r<-Roc(Conn ~ Temporal + Spatial + Entry + Goods + Trace + Victim, data = s)
plot(r,xlab="False positivity rate",ylab="True positivity rate")
click.Roc(r)
myplot<-calibrate(fimodel,method = "boot", B=100)
par(bg="white", las=1)
plot(myplot, las=1)
